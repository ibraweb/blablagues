//https://api.blablagues.net/?rub=blagues
const header = document.getElementById('header');
const text = document.getElementById('content');

const blablague = () => {
    fetch("https://api.blablagues.net/?rub=blagues").then((response) => response.json())
        .then((data) => {
            const content = data.data.content;
            header.textContent = content.text_head;
                text.textContent =
                    content.text !== ""
                ? content.text
                : content.text_hidden;
        });
};
blablague();

document.body.addEventListener('click', blablague);

